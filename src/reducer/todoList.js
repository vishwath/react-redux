import {createSlice} from '@reduxjs/toolkit'

const initialState = {
    todoList:[]
}
const todos = createSlice({
    name: "todoList",
    initialState,
    reducers: {
        saveTask: (state,action) => {
            state.todoList.push(action.payload)
        }
    }
});

export const {saveTask} = todos.actions
export const selectTodoList = state => state.todos.todoList
export default todos.reducer