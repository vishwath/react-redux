import {configureStore} from "@reduxjs/toolkit";
import todoReducer from './reducer/todoList'

export const store = configureStore({
    reducer:{
        todos: todoReducer
    }
})