import TaskList from "./components/TaskList";
import NewTaskForm from "./components/NewTaskForm";

function App() {
  return (
    <div className="App">
      <TaskList/>
      <NewTaskForm/>
    </div>
  );
}

export default App;
