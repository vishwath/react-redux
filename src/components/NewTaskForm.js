import {useState} from "react";
import {useDispatch} from "react-redux";
import {saveTask} from "../reducer/todoList";

function NewTaskForm() {
    const [task,setTask] = useState('');
    const dispatch = useDispatch();

    const addTask = () => {
        console.log(task)
        dispatch(saveTask({
            name: task,
            id: Date.now()
        }))
    }
    return (<div>
            <label>New Task</label>
            <input type="text" value={task} onChange={evt=>setTask(evt.target.value)}/>
            <button onClick={addTask}> </button>
    </div>)
}

export default NewTaskForm;