import {useSelector} from "react-redux";
import {selectTodoList} from "../reducer/todoList";

export default function TaskList() {
    const todoList = useSelector(selectTodoList)
    return <div>
        <h1>TaskList</h1>
        {
            todoList.map((todo) =>{
                return <li>{todo.name}</li>
            })
        }
    </div>
}